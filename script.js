//random integer generator found at:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

//roll one six-sided die and return the result
function roll1d6() {
  return getRandomIntInclusive(1, 6);
}

//roll two six-sided dice and return the result
function roll2d6() {
  return getRandomIntInclusive(2, 12);
}

//roll specified dice a certain number of times and return the "count" array
function countRolls(dice, repeat) {
  var count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  var i = 1;
  while (i <= repeat) {
    var current = dice();
    count[current] = count[current] + 1;
    i++;
  }
  return count;
}

//print the specified array
function drawCount(counted) {
  var length = counted.length;
  var i = 1;
  while (i < length) {
    if (counted[i] !== 0) {
      // console.log(i + ": " + counted[i]);
      var newElement = document.createElement("div");
      var newValue = counted[i];
      var newText = document.createTextNode(
        "\u00a0" + i + ":" + "\u00a0" + newValue
      );
      newElement.appendChild(newText);
      if (i % 2 === 0) {
        newElement.style.backgroundColor = "#AAAAAA";
      } else {
        newElement.style.backgroundColor = "#777777";
      }
      newElement.style.width = 0 + counted[i] + "px";
      var destionation = document.getElementById("d1");
      destionation.appendChild(newElement);
    }
    i++;
  }
}

//Roll 2d6 1000 times
var stats = countRolls(roll2d6, 1000);
console.log(stats);

//Print the results
drawCount(stats);
